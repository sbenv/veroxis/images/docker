#!/bin/sh

sed '/tonistiigi\/binfmt/!d' Dockerfile | tail -n 1 | cut -d':' -f2 | cut -d'v' -f2 | cut -d'-' -f1
