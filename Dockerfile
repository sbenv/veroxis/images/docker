FROM docker:27.1.2

COPY --from=tonistiigi/binfmt:buildkit-v6.2.0-24 /buildkit-qemu-* /usr/local/bin/
COPY --from=docker/buildx-bin:0.16.2 /buildx /usr/libexec/docker/cli-plugins/docker-buildx
